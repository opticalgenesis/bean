package com.lowpolybutt.beans

import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.PopupMenu
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.lowpolybutt.beans.dialogs.AddNewCoffeeDialog
import com.lowpolybutt.beans.dialogs.AddNewEspressoMachineDialog
import com.lowpolybutt.beans.dialogs.AddNewGrinderDialog
import com.lowpolybutt.beans.dialogs.NewBrewBottomSheet
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BeansApplication : Application()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("ACTIVITY", "hewwo")
        val tb: MaterialToolbar = findViewById(R.id.activity_main_top_app_bar)
        setSupportActionBar(tb)
        val collapsingToolbarLayout: CollapsingToolbarLayout = findViewById(R.id.activity_main_collapsing_toolbar_layout)
        val fab: ExtendedFloatingActionButton = findViewById(R.id.activity_main_add_new_item)
        fab.setOnClickListener { showAddNewItemMenu(it, R.menu.menu_new_item_add) }
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.activity_main_nav_host) as NavHostFragment
        val drawerLayout: DrawerLayout = findViewById(R.id.activity_main_drawer)
        val navController = navHostFragment.navController
        val appBarConfig = AppBarConfiguration(setOf(R.id.mainFragment, R.id.coffeeViewFragment, R.id.brewViewFragment), drawerLayout = drawerLayout)
        collapsingToolbarLayout.setupWithNavController(tb, navController, appBarConfig)

        val navigationView: NavigationView = findViewById(R.id.activity_main_navigation_view)
        navigationView.setNavigationItemSelectedListener {
            Log.d("ACTIVITY", "clicked")
            drawerLayout.close()
            navigationView.setCheckedItem(it)
            it.onNavDestinationSelected(navController)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.debug_overflow_menu, menu)
        return true
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        Log.d("MENU", "ITEM SELECTED")
//        return item.onNavDestinationSelected(findNavController(R.id.activity_main_nav_host)) || super.onOptionsItemSelected(item)
//    }

    private fun showAddNewItemMenu(v: View, @MenuRes menuRes: Int) {
        Log.d("BEAN", "Showing menu")
        val popup = PopupMenu(this, v)
        popup.menuInflater.inflate(menuRes, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.add_new_coffee_bean -> {
                    AddNewCoffeeDialog(supportFragmentManager).show(supportFragmentManager,
                    "TAG_ADD_NEW_COFFEE")
                    true
                }
                R.id.add_new_coffee_machine -> {
                    AddNewEspressoMachineDialog().show(supportFragmentManager, "TAG_ADD_NEW_ESPRESSO_MACHINE")
                    true
                }
                R.id.add_new_coffee_grinder -> {
                    AddNewGrinderDialog().show(supportFragmentManager, "TAG_ADD_NEW_GRINDER")
                    true
                }
                R.id.add_new_brew -> {
                    NewBrewBottomSheet().show(supportFragmentManager, NewBrewBottomSheet.TAG)
                    true
                }
                else -> false
            }
        }
        popup.show()
    }
}

interface OnLastBrewFetchedListener {
    suspend fun onLastBrewFetched(pair: Pair<Long, Int>)
}

interface OnBrewRatedListener {
    fun onBrewRated(rating: Double)
}