package com.lowpolybutt.beans.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Grinder(
    @PrimaryKey(autoGenerate = true) val uid: Long,
    val manufacturer: String,
    val model: String
) {
    override fun toString(): String {
        return "$manufacturer $model"
    }
}