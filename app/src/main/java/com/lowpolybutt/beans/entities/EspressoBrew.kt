package com.lowpolybutt.beans.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

// TODO this can be a master Entity for every brew type -- just needs renaming

@Entity
data class EspressoBrew(
    @PrimaryKey(autoGenerate = true) var uid: Long,
    val coffeeUid: Long,
    val machineUid: Long,
    val grinderUid: Long,
    val weightIn: Float,
    val weightOut: Float,
    val timeTakenInSeconds: Long,
    val brewTemperature: Int?,
    val grindSetting: Int?,
    val preInfusionTime: Long?,
    var rating: Double = 0.0
)