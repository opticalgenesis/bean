package com.lowpolybutt.beans.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Coffee(
    @PrimaryKey(autoGenerate = true) val uid: Long,
    val name: String,
    val bIsSingleOrigin: Boolean = true,
    val origins: List<String>,
    val roaster: String,
    val roastedOn: Long?,
    val deliveredOn: Long?
) {
    override fun toString(): String = name
}