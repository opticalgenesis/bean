package com.lowpolybutt.beans.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EspressoMachine(
    @PrimaryKey(autoGenerate = true) val uid: Long,
    val model: String,
    val manufacturer: String,
    val bHasBuiltinGrinder: Boolean = false
) {
    override fun toString(): String {
        return "$manufacturer $model"
    }
}