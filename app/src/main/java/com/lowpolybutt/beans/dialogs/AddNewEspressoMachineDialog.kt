package com.lowpolybutt.beans.dialogs

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.CheckBox
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.EspressoMachineDatabase
import com.lowpolybutt.beans.GrinderDatabase
import com.lowpolybutt.beans.R
import com.lowpolybutt.beans.dao.EspressoMachineDao
import com.lowpolybutt.beans.entities.EspressoMachine
import com.lowpolybutt.beans.entities.Grinder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddNewEspressoMachineDialog : DialogFragment() {
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = AppDatabase.getInstance(requireActivity())
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_new_espresso_machine, null, false)
        val machineManufacturerInput: TextInputEditText = v.findViewById(R.id.dialog_new_espresso_machine_input)
        val machineModelInput: TextInputEditText = v.findViewById(R.id.dialog_new_espresso_machine_model_input)

        val machineManufacturerLayout: TextInputLayout = v.findViewById(R.id.dialog_new_espresso_machine_layout)
        val machineModelLayout: TextInputLayout = v.findViewById(R.id.dialog_new_espresso_machine_model_layout)

        machineManufacturerLayout.apply {
            isErrorEnabled = true
            error = "Field is required"
            editText?.doOnTextChanged { _, _, _, count ->
                if (count < 1) {
                    isErrorEnabled = true
                    error = "Field is required"
                } else {
                    isErrorEnabled = false
                }
            }
        }

        machineModelLayout.apply {
            isErrorEnabled = true
            error = "Field is required"
            editText?.doOnTextChanged { text, _, _, count ->
                Log.d("BEAN_TAG", text?.toString() ?: "Error")
                if (count < 1) {
                    isErrorEnabled = true
                    error = "Field is required"
                } else {
                    isErrorEnabled = false
                }
            }
        }

        val dao = db.espressoMachineDao()
        val grinderDao = db.grinderDao()

        val hasGrinder: CheckBox = v.findViewById(R.id.dialog_new_espresso_machine_has_grinder)

        return MaterialAlertDialogBuilder(requireContext())
            .setView(v)
            .setNegativeButton("Cancel") {_, _ ->
                dismiss()
            }
            .setPositiveButton("Add") {_, _ ->
                val machine = EspressoMachine(
                    System.currentTimeMillis(),
                    model = machineModelInput.editableText.toString(),
                    manufacturer = machineManufacturerInput.editableText.toString(),
                    bHasBuiltinGrinder = hasGrinder.isChecked)
                Log.d("BEAN_TAG", machine.toString())
                lifecycleScope.launch(Dispatchers.IO) {
                    dao.addNewEspressoMachine(machine)
                    if (hasGrinder.isChecked) {
                        grinderDao.addNewGrinder(Grinder(
                            System.currentTimeMillis(),
                            manufacturer = machineManufacturerInput.editableText.toString(),
                            model = machineModelInput.editableText.toString()
                        ))
                    }
                }
            }
            .setTitle("Add New Espresso Machine").create()
    }
}