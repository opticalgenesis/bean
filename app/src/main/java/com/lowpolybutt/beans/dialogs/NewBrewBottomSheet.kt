package com.lowpolybutt.beans.dialogs

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.lowpolybutt.beans.NewEspressoBrewActivity
import com.lowpolybutt.beans.R

class NewBrewBottomSheet : BottomSheetDialogFragment() {
    companion object {
        const val TAG = "BOTTOM_SHEET_DIALOG_TAG"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.bsd_new_brew, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val newEspressoButton = view.findViewById<Button>(R.id.bsd_new_brew_espresso)
        val newPourButton = view.findViewById<Button>(R.id.bsd_new_brew_pourover)
        val newMokaButton = view.findViewById<Button>(R.id.bsd_new_brew_moka)

        newPourButton.setOnClickListener { Toast.makeText(requireContext(), "Coming soon...", Toast.LENGTH_SHORT).show() }
        newMokaButton.setOnClickListener { Toast.makeText(requireContext(), "Coming soon...", Toast.LENGTH_SHORT).show() }

        newEspressoButton.setOnClickListener {
            requireActivity().startActivity(Intent(requireActivity(), NewEspressoBrewActivity::class.java))
            dismiss()
        }
    }
}