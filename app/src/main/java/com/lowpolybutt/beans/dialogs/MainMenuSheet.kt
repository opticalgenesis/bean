package com.lowpolybutt.beans.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.lowpolybutt.beans.R

class MainMenuSheet : BottomSheetDialogFragment() {
    companion object {
        const val TAG = "NAV_SHEET_DIALOG"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.bsd_main_menu, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewBrews: Button = view.findViewById(R.id.activity_main_menu_view_brews)
    }
}