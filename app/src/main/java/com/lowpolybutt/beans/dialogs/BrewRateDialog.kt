package com.lowpolybutt.beans.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.widget.RatingBar
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lowpolybutt.beans.OnBrewRatedListener
import com.lowpolybutt.beans.R

class BrewRateDialog(private val ratingSelectedListener: OnBrewRatedListener) : DialogFragment() {

    private var rating: Double = 0.0

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = layoutInflater.inflate(R.layout.dialog_rate_brew, null, false)
        val ratingBar = v.findViewById<AppCompatRatingBar>(R.id.dialog_brew_rate_bar)
        ratingBar.onRatingBarChangeListener =
            RatingBar.OnRatingBarChangeListener { _, value, _ -> rating = value.toDouble() }
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle("How was this brew?")
            .setPositiveButton("Rate") { _, _ -> ratingSelectedListener.onBrewRated(rating) }
            .create()
    }
}