package com.lowpolybutt.beans.dialogs

import android.app.Dialog
import android.icu.number.NumberFormatter
import android.icu.number.Precision
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.room.PrimaryKey
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.common.base.Stopwatch
import com.lowpolybutt.beans.R
import com.lowpolybutt.beans.entities.EspressoBrew
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit

class NewEspressoBrewDialog(private val brewFinishedListener: OnBrewFinishedListener) : DialogFragment(), OnTimeTickedListener {

    private lateinit var stopwatch: Stopwatch

    private var elapsedTime: Long = 0

    private lateinit var timeView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        stopwatch = Stopwatch.createUnstarted()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_new_espresso_brew, null)
        val finalWeightLayout: TextInputLayout = view.findViewById(R.id.dialog_new_espresso_brew_final_weight_layout)
        val finalWeightInput: TextInputEditText = view.findViewById(R.id.dialog_new_espresso_brew_final_weight_input)
        timeView = view.findViewById(R.id.dialog_new_espresso_brew_timer_view)
        val stopTimer: Button = view.findViewById(R.id.dialog_new_espresso_brew_stop_timer)
        // Need to add assertion stopwatch has started prior
        stopTimer.setOnClickListener {
            stopwatch.stop()
            finalWeightLayout.visibility = View.VISIBLE
        }
        val resetTimer: Button = view.findViewById(R.id.dialog_new_espresso_brew_reset_timer)
        resetTimer.setOnClickListener {
            stopwatch.reset()
            elapsedTime = 0
            updateTime(elapsedTime)
        }
        val startTimer: Button = view.findViewById(R.id.dialog_new_espresso_brew_start_timer)
        startTimer.setOnClickListener {
            startStopwatch(this)
            stopTimer.isEnabled = true
            resetTimer.isEnabled = true
        }
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle("Timer")
            .setView(view)
            .setPositiveButton("Continue") { _, _ ->
                brewFinishedListener.brewFinished(elapsedTime, finalWeightInput.editableText.toString().toFloat())
            }
            .setNegativeButton("Cancel") { _, _ -> dismiss() }
            .create()
    }

    override fun updateTime(newElapsedTime: Long) {
        Log.d("BEAN_TAG", "Time elapsed: $newElapsedTime")
        timeView.text = TimeUnit.MILLISECONDS.toSeconds(newElapsedTime).toString()
    }

    private fun startStopwatch(li: OnTimeTickedListener) {
        lifecycleScope.launch(Dispatchers.IO) {
            stopwatch.start()
            while (stopwatch.isRunning) {
                elapsedTime = stopwatch.elapsed(TimeUnit.MILLISECONDS)
                li.updateTime(elapsedTime)
            }
            cancel()
        }
    }

    interface OnBrewFinishedListener {
        fun brewFinished(brewTime: Long, finalWeight: Float)
    }
}

private interface OnTimeTickedListener {
    fun updateTime(newElapsedTime: Long)
}