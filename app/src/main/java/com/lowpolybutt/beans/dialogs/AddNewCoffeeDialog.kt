package com.lowpolybutt.beans.dialogs

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.R
import com.lowpolybutt.beans.entities.Coffee
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class AddNewCoffeeDialog(private val supportFragmentManager: FragmentManager) : DialogFragment() {
    private lateinit var db: AppDatabase

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        db = AppDatabase.getInstance(requireActivity())
        val dialogView =
            layoutInflater.inflate(R.layout.dialog_new_coffee_bean_layout, null, false)
        val beanNameInput: TextInputEditText =
            dialogView.findViewById(R.id.dialog_new_coffee_bean_name_input)
        val originInputLayout: TextInputLayout =
            dialogView.findViewById(R.id.dialog_new_bean_origin)
        val originInput: TextInputEditText =
            dialogView.findViewById(R.id.dialog_new_bean_origin_input)
        val roasterInput: TextInputEditText =
            dialogView.findViewById(R.id.dialog_new_bean_roaster_input)

        originInputLayout.setEndIconOnClickListener {
            Toast.makeText(requireActivity(), "TODO, check back later", Toast.LENGTH_LONG).show()
        }

        val roastedOnButton: Button =
            dialogView.findViewById(R.id.dialog_new_bean_select_roast_date_button)
        val deliveredOnButton: Button =
            dialogView.findViewById(R.id.dialog_new_bean_select_delivered_date_button)

        var roastedOnInMillis: Long? = null
        var deliveredOnInMillis: Long? = null

        val roastedOnPicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select Roast Date")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build().apply {
                addOnPositiveButtonClickListener {
                    roastedOnButton.text = parseSelectedDate(it)
                    roastedOnInMillis = it
                }
                addOnCancelListener {
                    roastedOnInMillis = null
                    roastedOnButton.text = "Select Roast Date"
                }
            }

        roastedOnButton.setOnClickListener {
            roastedOnPicker.show(
                supportFragmentManager,
                "ROAST_DATE_PICKER_TAG"
            )
        }

        val deliveredOnPicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select Delivered Date")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build().apply {
                addOnPositiveButtonClickListener {
                    deliveredOnButton.text = parseSelectedDate(it)
                    deliveredOnInMillis = it
                }
                addOnCancelListener {
                    deliveredOnInMillis = null
                    deliveredOnButton.text = "Select Delivered Date"
                }
            }

        deliveredOnButton.setOnClickListener {
            deliveredOnPicker.show(
                supportFragmentManager,
                "DELIV_DATE_PICKER_TAG"
            )
        }

        val coffeeDao = db.coffeeDao()

        return MaterialAlertDialogBuilder(requireActivity())
            .setView(dialogView)
            .setTitle("Add New Coffee")
            .setNegativeButton("Cancel") { _, _ ->
                dismiss()
            }
            .setPositiveButton("Add") { _, _ ->
                Log.d("BEAN", "Inserting new coffee bean")
                lifecycleScope.launch(Dispatchers.IO) {
                    coffeeDao.addNewCoffeeBean(
                        Coffee(
                            System.currentTimeMillis(),
                            beanNameInput.editableText.toString(),
                            bIsSingleOrigin = false,
                            origins = listOf(originInput.editableText.toString()),
                            roaster = roasterInput.editableText.toString(),
                            roastedOn = roastedOnInMillis,
                            deliveredOn = deliveredOnInMillis
                        )
                    )
                }
            }.create()

    }
    private fun parseSelectedDate(timeInMillis: Long): String {
        val formatter = SimpleDateFormat.getDateInstance()
        return formatter.format(Date(timeInMillis))
    }
}