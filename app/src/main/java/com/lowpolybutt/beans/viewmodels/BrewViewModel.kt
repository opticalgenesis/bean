package com.lowpolybutt.beans.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.entities.EspressoBrew

class BrewViewModel(private val db: AppDatabase) : ViewModel() {
    private val allBrews: MutableLiveData<List<EspressoBrew>> by lazy {
        MutableLiveData<List<EspressoBrew>>().also { loadBrews() }
    }

    fun getBrews() = allBrews

    private fun loadBrews() {
        db.brewDao().getAllEspressoBrewsLiveData()
    }
}