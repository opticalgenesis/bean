package com.lowpolybutt.beans.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.entities.Coffee

class CoffeeViewModel(private val db: AppDatabase, private val uid: Long) : ViewModel() {
    private val allCoffees: MutableLiveData<List<Coffee>> by lazy {
        MutableLiveData<List<Coffee>>().also { loadCoffees() }
    }

    private val mutableCoffee: MutableLiveData<Coffee> by lazy {
        MutableLiveData<Coffee>().also { loadCoffeeForUid(uid) }
    }

    fun getCoffee() = mutableCoffee

    fun getCoffees() = allCoffees

    private fun loadCoffeeForUid(uid: Long) {
        db.coffeeDao().getCoffeeForUidLiveData(uid)
    }

    private fun loadCoffees() {
        db.coffeeDao().getAllCoffeeBeansLiveData()
    }
}