package com.lowpolybutt.beans.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.OnLastBrewFetchedListener
import com.lowpolybutt.beans.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainFragment : Fragment(), OnLastBrewFetchedListener {

    private val db by lazy {
        AppDatabase.getInstance(requireContext())
    }

    private lateinit var lastBrewTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lastBrewTextView = view.findViewById(R.id.fragment_main_last_brew_content)
        lifecycleScope.launch(Dispatchers.IO) { loadLastBrewForGreeting() }
        db.brewDao().getAllEspressoBrewsLiveData().observe(requireActivity()) {
            Log.d("BEAN_TAG", "There are ${it.size} brews")
        }
    }

    override suspend fun onLastBrewFetched(pair: Pair<Long, Int>) {
        TODO("Not yet implemented")
    }

    private suspend fun loadLastBrewForGreeting() {
        val brewDao = db.brewDao()
        val coffeeDao = db.coffeeDao()
        val machineDao = db.espressoMachineDao()
        val grinderDao = db.grinderDao()

        if (brewDao.getAllEspressoBrews().isNotEmpty()) {
            val lastBrew = brewDao.getAllEspressoBrews()[0]
            val lastCoffee = coffeeDao.getCoffeeForUid(lastBrew.coffeeUid)
            val lastMachine = machineDao.getEspressoMachineForUid(lastBrew.machineUid)
            val lastGrinder = grinderDao.getGrinderForUid(lastBrew.grinderUid)

            val brew = "${lastCoffee.name} brewed on your ${lastMachine.model} with your ${lastGrinder.model} grinder. ${lastBrew.weightIn} gave ${lastBrew.weightOut} in ${lastBrew.timeTakenInSeconds}. You rated this ${lastBrew.rating}/5"
            lifecycleScope.launch(Dispatchers.Main) { displayGreeting(brew) }
        } else {
            lifecycleScope.launch(Dispatchers.Main) { displayGreeting("Nothing here yet, get brewing!") }
        }
    }

    private fun displayGreeting(greeting: String) {
        lastBrewTextView.text = greeting
    }
}