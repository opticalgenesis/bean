package com.lowpolybutt.beans.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.R
import com.lowpolybutt.beans.entities.Coffee

class CoffeeViewFragment : Fragment() {
    private lateinit var rv: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("COFFEE", "hewwo")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_brews_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv = view.findViewById(R.id.fragment_brews_view_rv)
        rv.adapter = CoffeeViewRecyclerViewAdapter()
        rv.layoutManager = LinearLayoutManager(requireContext())

        val db = AppDatabase.getInstance(requireContext())
        db.coffeeDao().getAllCoffeeBeansLiveData().observe(requireActivity()) {
            (rv.adapter as CoffeeViewRecyclerViewAdapter).updateList(it)
            Log.d("COFFEE", "There are ${it.size} coffees")
        }
    }

    inner class CoffeeViewRecyclerViewAdapter : RecyclerView.Adapter<CoffeeViewRecyclerViewAdapter.ViewHolder>() {
        private var coffeeList: List<Coffee> = emptyList()

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val coffeeInfo: TextView = view.findViewById(R.id.brew_item_coffee)
        }

        override fun getItemCount(): Int = coffeeList.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            Log.d("COFFEE", "Binding item at position $position")
            holder.coffeeInfo.text = coffeeList[position].name
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(LayoutInflater.from(requireContext()).inflate(R.layout.rv_item_brew_view, null, false))

        fun updateList(list: List<Coffee>) {
            coffeeList = list
            notifyDataSetChanged()
        }
    }
}