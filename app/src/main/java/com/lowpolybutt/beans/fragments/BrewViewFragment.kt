package com.lowpolybutt.beans.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lowpolybutt.beans.AppDatabase
import com.lowpolybutt.beans.R
import com.lowpolybutt.beans.entities.EspressoBrew

class BrewViewFragment : Fragment() {
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = AppDatabase.getInstance(requireActivity())
        Log.d("BREWS", "hewwo")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_brews_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv: RecyclerView = view.findViewById(R.id.fragment_brews_view_rv)
        rv.layoutManager = LinearLayoutManager(requireContext())
        val adapter = BrewViewRecyclerViewAdapter(this)
        rv.adapter = adapter

        db.brewDao().getAllEspressoBrewsLiveData().observe(requireActivity()) {
            adapter.updateList(it)
        }
    }

    inner class BrewViewRecyclerViewAdapter(private val ctx: Fragment) : RecyclerView.Adapter<BrewViewRecyclerViewAdapter.ViewHolder>() {

        private var brewsList: List<EspressoBrew> = emptyList()

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val coffeeName: TextView = view.findViewById(R.id.brew_item_coffee)
            val equipmentName: TextView = view.findViewById(R.id.activity_brew_item_details_equipment)
            val grinderName: TextView = view.findViewById(R.id.activity_brew_item_details_grinder)
            val ratingView: TextView = view.findViewById(R.id.activity_brew_item_details_rating)

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_item_brew_view, parent, false)
            return ViewHolder(v)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val db = AppDatabase.getInstance(ctx.requireActivity())
            db.coffeeDao().getCoffeeForUidLiveData(brewsList[position].coffeeUid).observe(ctx, Observer {
                holder.coffeeName.text = it.name
            })
            db.espressoMachineDao().getEspressoMachineForUidLiveData(brewsList[position].machineUid).observe(ctx) {
                holder.equipmentName.text = it.toString()
            }
            db.grinderDao().getGrinderForUidLiveData(brewsList[position].grinderUid).observe(ctx) {
                holder.grinderName.text = it.toString()
            }
        }

        override fun getItemCount(): Int = brewsList.size

        fun updateList(list: List<EspressoBrew>) {
            brewsList = list
            notifyDataSetChanged()
        }
    }
}