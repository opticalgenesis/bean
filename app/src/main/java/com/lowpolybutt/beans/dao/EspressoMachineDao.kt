package com.lowpolybutt.beans.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lowpolybutt.beans.entities.EspressoMachine

@Dao
interface EspressoMachineDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewEspressoMachine(machine: EspressoMachine)

    @Query("SELECT * FROM espressomachine")
    fun getAllEspressoMachines(): List<EspressoMachine>

    @Query("SELECT * FROM espressomachine WHERE uid LIKE :uid")
    suspend fun getEspressoMachineForUid(uid: Long): EspressoMachine

    @Query("SELECT * FROM espressomachine WHERE uid LIKE :uid")
    fun getEspressoMachineForUidLiveData(uid: Long): LiveData<EspressoMachine>

    @Query("DELETE FROM espressomachine")
    fun clearTable()
}