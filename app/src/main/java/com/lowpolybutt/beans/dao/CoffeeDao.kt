package com.lowpolybutt.beans.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.lowpolybutt.beans.entities.Coffee

@Dao
interface CoffeeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewCoffeeBean(coffee: Coffee)

    @Query("SELECT * FROM coffee")
    fun getAllCoffeeBeans(): List<Coffee>

    @Query("SELECT * FROM coffee")
    fun getAllCoffeeBeansLiveData(): LiveData<List<Coffee>>

    @Query("SELECT * FROM coffee WHERE name LIKE :name")
    fun getCoffeeBeanForName(name: String): List<Coffee>

    @Query("SELECT * FROM coffee WHERE uid LIKE :uid")
    fun getCoffeeForUidLiveData(uid: Long): LiveData<Coffee>

    @Query("SELECT * FROM coffee WHERE origins IN (:origins)")
    fun getCoffeeBeansForOrigin(origins: List<String>): List<Coffee>

    @Query("SELECT * FROM coffee WHERE uid LIKE :uid")
    suspend fun getCoffeeForUid(uid: Long): Coffee

    @Query("DELETE FROM coffee")
    fun clearTable()

    @Delete
    fun deleteCoffeeBean(coffee: Coffee)
}