package com.lowpolybutt.beans.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lowpolybutt.beans.entities.EspressoBrew

@Dao
interface EspressoBrewDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewEspressoBrew(brew: EspressoBrew)

    @Query("SELECT * FROM espressobrew")
    fun getAllEspressoBrews(): List<EspressoBrew>

    @Query("SELECT * FROM espressobrew")
    fun getAllEspressoBrewsLiveData(): LiveData<List<EspressoBrew>>

    @Query("SELECT * FROM espressobrew WHERE uid LIKE :uid")
    suspend fun getBrewForUid(uid: Long): EspressoBrew

    @Query("DELETE FROM espressobrew")
    fun clearTable()
}