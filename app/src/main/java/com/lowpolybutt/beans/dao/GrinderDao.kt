package com.lowpolybutt.beans.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lowpolybutt.beans.entities.Grinder

@Dao
interface GrinderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewGrinder(grinder: Grinder)

    @Query("SELECT * FROM grinder")
    fun getAllGrinders(): List<Grinder>

    @Query("SELECT * FROM grinder WHERE manufacturer LIKE :manufacturer")
    fun getAllGrindersForManufacturer(manufacturer: String): List<Grinder>

    @Query("SELECT * FROM grinder WHERE model LIKE :model")
    fun getGrinderFromModelName(model: String): List<Grinder>

    @Query("SELECT * FROM grinder WHERE uid LIKE :uid")
    suspend fun getGrinderForUid(uid: Long): Grinder

    @Query("SELECT * FROM grinder WHERE uid LIKE :uid")
    fun getGrinderForUidLiveData(uid: Long): LiveData<Grinder>

    @Query("DELETE FROM grinder")
    fun clearTable()

}