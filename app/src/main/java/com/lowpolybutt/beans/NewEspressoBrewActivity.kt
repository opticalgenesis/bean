package com.lowpolybutt.beans

import android.os.Bundle
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.lifecycleScope
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.lowpolybutt.beans.dialogs.BrewRateDialog
import com.lowpolybutt.beans.dialogs.NewEspressoBrewDialog
import com.lowpolybutt.beans.entities.Coffee
import com.lowpolybutt.beans.entities.EspressoBrew
import com.lowpolybutt.beans.entities.EspressoMachine
import com.lowpolybutt.beans.entities.Grinder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewEspressoBrewActivity : AppCompatActivity(), NewEspressoBrewDialog.OnBrewFinishedListener,
                                    OnBrewRatedListener {
    private val db = AppDatabase.getInstance(this)

    private var bHasSelectedMachine: Boolean = false
    private var bHasSelectedCoffee: Boolean = false
    private var bHasSelectedGrinder: Boolean = false
    private var bHasSelectedInputWeight: Boolean = false

    private var inWeight: Float = 0.0f

    private lateinit var selectedCoffee: Coffee
    private lateinit var selectedGrinder: Grinder
    private lateinit var selectedMachine: EspressoMachine

    private lateinit var brewToCommit: EspressoBrew

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_espresso_brew)
        val topBar: MaterialToolbar = findViewById(R.id.activity_new_espresso_toolbar)
        setSupportActionBar(topBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        lifecycleScope.launch(Dispatchers.IO) {
            Looper.prepare()
            populateAutoCompleteTextViews()
        }

        val weightInput: TextInputEditText = findViewById(R.id.activity_new_espresso_weight_in_input)
        val weightInputLayout: TextInputLayout = findViewById(R.id.activity_new_espresso_weight_in_layout)
        weightInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // Not needed
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // Not needed
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    if (p0.isNotEmpty()) {
                        weightInputLayout.isErrorEnabled = !weightInputLayout.isErrorEnabled
                        inWeight = p0.toString().toFloat()
                        bHasSelectedInputWeight = true
                        validateRequiredInputs()
                    } else {
                        weightInputLayout.isErrorEnabled = !weightInputLayout.isErrorEnabled
                        weightInputLayout.error = "Required"
                        bHasSelectedInputWeight = false
                        validateRequiredInputs()
                    }
                }
            }
        })
    }

    override fun brewFinished(brewTime: Long, finalWeight: Float) {
        // TODO
        brewToCommit = EspressoBrew(0, selectedCoffee.uid, selectedMachine.uid,
        selectedGrinder.uid, inWeight, finalWeight, brewTime, null, null, null)
        BrewRateDialog(this).show(supportFragmentManager, "")
    }

    override fun onBrewRated(rating: Double) {
        val brewDb = AppDatabase.getInstance(this)
        val dao = brewDb.brewDao()
        val uid = System.currentTimeMillis()
        brewToCommit.uid = uid
        brewToCommit.rating = rating
        Log.d("BEAN_TAG", "Committing details to prefs")
        lifecycleScope.launch(Dispatchers.IO) {
            dao.addNewEspressoBrew(brewToCommit)
        }
    }

    private suspend fun saveLastBrewDetails(brewUid: Long) {
        dataStore.edit { settings ->
            Log.d("BEAN_TAG", "Writing uid")
            settings[BrewType.LAST_BREW_UID_KEY] = brewUid
            settings[BrewType.LAST_BREW_TYPE_KEY] = BrewType.ESPRESSO
        }
        Log.d("BEAN_TAG", "UID WRITTEN")
    }

    private suspend fun populateAutoCompleteTextViews() {
        val allCoffees = db.coffeeDao().getAllCoffeeBeans()
        val allMachines = db.espressoMachineDao().getAllEspressoMachines()
        val allGrinders = db.grinderDao().getAllGrinders()

        // Grinder View setups
        val grinderSelectorView: AutoCompleteTextView = findViewById(R.id.activity_new_espresso_grinder_selector)
        val grinderAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, allGrinders)
        grinderSelectorView.setAdapter(grinderAdapter)
        grinderSelectorView.setOnItemClickListener { adapterView, view, i, l ->
            selectedGrinder = adapterView.getItemAtPosition(i) as Grinder
            bHasSelectedGrinder = true
            validateRequiredInputs()
        }

        // Machine View setups
        val machineSelectorView: AutoCompleteTextView = findViewById(R.id.activity_new_espresso_machine_selector)
        val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, allMachines)
        machineSelectorView.setAdapter(adapter)
        machineSelectorView.setOnItemClickListener { adapterView, view, i, l ->
            selectedMachine = adapterView.getItemAtPosition(i) as EspressoMachine
            bHasSelectedMachine = true
            validateRequiredInputs()
        }

        val coffeeSelectorView: AutoCompleteTextView = findViewById(R.id.activity_new_espresso_coffee_selector)
        val coffeeAdaptor = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, allCoffees)
        coffeeSelectorView.setAdapter(coffeeAdaptor)
        coffeeSelectorView.setOnItemClickListener { adapterView, _, i, _ ->
            selectedCoffee = adapterView.getItemAtPosition(i) as Coffee
            bHasSelectedCoffee = true
            validateRequiredInputs()
        }
    }

    private fun validateRequiredInputs() {
        val beginButton: Button = findViewById(R.id.activity_new_espresso_start_button)
        if (bHasSelectedMachine && bHasSelectedCoffee && bHasSelectedGrinder && bHasSelectedInputWeight) {
            beginButton.isEnabled = true
            beginButton.setOnClickListener {
                Log.d("BEAN_TAG", "Selected tings are: Grinder: $selectedGrinder\nCoffee: $selectedCoffee\nMachine: $selectedMachine\nWeight: $inWeight")
                NewEspressoBrewDialog(this).show(supportFragmentManager, "")
            }
        } else {
            beginButton.isEnabled = false
        }
    }
}