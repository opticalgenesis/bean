package com.lowpolybutt.beans

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.lowpolybutt.beans.entities.Coffee
import javax.inject.Inject

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class BrewType {
    companion object {
        const val ESPRESSO = 0
        const val POUROVER = 1
        const val MOKA = 2

        val LAST_BREW_UID_KEY = longPreferencesKey("last_brew_uid")
        val LAST_BREW_TYPE_KEY = intPreferencesKey("last_brew_type")
    }
}
