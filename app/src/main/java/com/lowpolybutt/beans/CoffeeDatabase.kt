package com.lowpolybutt.beans

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.lowpolybutt.beans.dao.CoffeeDao
import com.lowpolybutt.beans.dao.EspressoBrewDao
import com.lowpolybutt.beans.dao.EspressoMachineDao
import com.lowpolybutt.beans.dao.GrinderDao
import com.lowpolybutt.beans.entities.Coffee
import com.lowpolybutt.beans.entities.EspressoBrew
import com.lowpolybutt.beans.entities.EspressoMachine
import com.lowpolybutt.beans.entities.Grinder

@Database(entities = [Coffee::class], version = 2, exportSchema = true)
@TypeConverters(Converters::class)
abstract class CoffeeDatabase : RoomDatabase() {
    abstract fun coffeeDao(): CoffeeDao
}

@Database(entities = [EspressoMachine::class], version = 2, exportSchema = true)
@TypeConverters(Converters::class)
abstract class EspressoMachineDatabase : RoomDatabase() {
    abstract fun espressoMachineDao(): EspressoMachineDao
}

@Database(entities = [Grinder::class], version = 2, exportSchema = true)
@TypeConverters(Converters::class)
abstract class GrinderDatabase : RoomDatabase() {
    abstract fun grinderDao(): GrinderDao
}

@Database(entities = [EspressoBrew::class], version = 2, exportSchema = true)
@TypeConverters(Converters::class)
abstract class EspressoBrewDatabase : RoomDatabase() {
    abstract fun espressoBrewDao(): EspressoBrewDao
}

@Database(entities = [Coffee::class, EspressoMachine::class, Grinder::class, EspressoBrew::class], version = 1, exportSchema = true)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun coffeeDao(): CoffeeDao
    abstract fun espressoMachineDao(): EspressoMachineDao
    abstract fun grinderDao(): GrinderDao
    abstract fun brewDao(): EspressoBrewDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(ctx: Context) = INSTANCE ?:
        Room.databaseBuilder(ctx, AppDatabase::class.java, "main-db")
            .fallbackToDestructiveMigration()
            .build().also { INSTANCE = it }
    }
}